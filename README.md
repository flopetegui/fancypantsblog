# FancyPants Engineer Blog

Blog about things that matter to those who think they engineer good! Atleast good enough so that they strut around as if they were wearing fancy pants.

## Getting Started

Blog platform is taken from https://jekyllrb.com/

### Prerequisites/Installation

Please reference the jekyllrb site for details on pre-req and installation instructions.
https://jekyllrb.com/docs/installation/

## Running

Jekyll gem should be installed on your system. It can serve locally by:

```
bundle exec jekyll serve
```

Browse to http://localhost:4000


### Testing

No, not yet!


## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Jekyll](https://jekyllrb.com/docs) - Opensource blog platform used

## Authors

* **Francis Lopetegui** - [Gitlab](https://gitlab.com/flopetegui)

## Acknowledgments

The following groups have inspired me throughout my career. In Northern Ireland, the seven members of the New Provo Front. In Canada, the five imprisoned leaders of Liberte de Quebec. In Sri Lanka, the nine members of the Asian Dawn movement.
