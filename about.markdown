---
layout: page
title: About
permalink: /about/
---

Software Engineer working in Toronto. Talking about a wide range of topics, but mostly technology related. This Blog is needed in order to be able to express thoughts and maybe some frustration. Work mostly on distributed system and attempting to further my understanding of how to best deliver product in the modern software industry.
